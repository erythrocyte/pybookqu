#!/usr/bin/env python3
# coding: utf-8


def factorial(n):
    """
    """
    if n == 0:
        return 1
    else:
        return n * factorial(n-1)
