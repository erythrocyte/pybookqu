#!/usr/bin/env python3
# coding: utf-8

from src import common


def do():
    n = 100
    fn = common.factorial(n)
    print (type(fn))
    print('{0} factorial is {1}'.format(n, fn))
    return digitsum(fn)


def digitsum(x):
    total = 0
    for letter in str(x):
        total += ord(letter) - 48
    return total 
