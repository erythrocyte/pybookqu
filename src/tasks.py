#!/usr/bin/env python3
# coding: utf-8

from src import t1, t2


def run_tasks(tasks):
    for it in tasks:
        if it == 0:
            r = t1.do()
            print(r)

        if it == 1:
            t2.do()
