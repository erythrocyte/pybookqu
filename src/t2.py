#!/usr/bin/env python3
# coding: utf-8

import urllib.request
import math
import time


def do():
    url_txt = "https://projecteuler.net/project/resources/p099_base_exp.txt"
    lines = get_lines_url(url_txt)

    max_val = 0
    max_ind = -1
    for ind, l in enumerate(lines):
        [a, b] = list(map(int, l.split(',')))
        val = math.log2(a) * b
        if val > max_val:
            max_ind = ind
            max_val = val

    if max_ind is not -1:
        [a, b] = list(map(int, lines[max_ind].split(',')))

    start = time. time()
    max_val = a ** b
    end = time. time()
    print('power calculation takes {0} sec'.format(end - start))

    print('for line index = {0}'.format(max_ind))


def get_lines_url(url_txt, printStatus=False):
    response = urllib.request.urlopen(url_txt)
    data = response.read()  # a `bytes` object
    text = data.decode('utf-8')  # a `str`; this step can't be used if data is binary
    lines = text.split('\n')

    if printStatus is True:
        print('lines count = {0}'.format(len(lines)))

    return lines
