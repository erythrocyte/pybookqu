#!/usr/bin/env python3
# coding: utf-8

from src import tasks

def main():
    t = [1]
    tasks.run_tasks(t)


if __name__ == '__main__':
    main()
